﻿using System;

namespace TestSiteAdmin.Models
{
    public class Reservacion
    {
        public string name;
        public string description;
        public DateTime start_date;
        public DateTime end_date;
        public string bo_mail;
        public string agency_mail;
        public string user_name;
        public string user_email;
        public string site;        
        public int id;
    }
}