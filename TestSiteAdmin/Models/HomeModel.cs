﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSiteAdmin.Models
{
    public class HomeModel
    {
        public List<Sitio> sitios;
        public List<Reservacion> reservaciones;

        public HomeModel()
        {
            this.sitios = new List<Sitio>();
            this.reservaciones = new List<Reservacion>();
        }
    }
}