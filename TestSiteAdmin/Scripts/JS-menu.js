﻿$(document).ready(function () {
    var x = location.pathname;
    switch (x) {
        case "/":
            $('.navbar-nav li a').css('color', '#333');
            $('.navbar-nav li:contains("Test")').css('background-color', '#00999a');
            $('.navbar-nav li a:contains("Test")').css('color', '#FFF');
            break;
        case "/reservations":
            $('.navbar-nav li a').css('color', '#333');
            $('.navbar-nav li a:contains("Reservations")').css('color', '#FFF');
            $('.navbar-nav li:contains("Reservations")').css('background-color', '#00999a');
            break;
        case "/users":
            $('.navbar-nav li a').css('color', '#333');
            $('.navbar-nav li a:contains("Users")').css('color', '#FFF');
            $('.navbar-nav li:contains("Users")').css('background-color', '#00999a');
            break;
        case "/sites":
            $('.navbar-nav li a').css('color', '#333');
            $('.navbar-nav li a:contains("Sites")').css('color', '#FFF');
            $('.navbar-nav li:contains("Sites")').css('background-color', '#00999a');
            break;
        case "/servers":
            $('.navbar-nav li a').css('color', '#333');
            $('.navbar-nav li a:contains("Servers")').css('color', '#FFF');
            $('.navbar-nav li:contains("Servers")').css('background-color', '#00999a');
            break;

        default:
            break;
    }

    $('.table td a:contains("Delete")').css('color', '#a74921');
    $('.table td a:contains("Edit")').css('color', '#b6ae3f');
    
         
  
});