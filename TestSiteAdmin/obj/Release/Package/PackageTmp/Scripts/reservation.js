﻿var allowSubmit = false;

$(function () {
    $("#frm").on("submit", function (e) {
        if (!allowSubmit) {
            e.preventDefault();
            $("#overlapExists").hide();
            var formData = "id_site=" + $("#id_site").val();
            formData += "&start_date=" + $("#start_date").val();
            formData += "&end_date=" + $("#end_date").val();
            if ($("#id").length > 0) {
                formData += "&id=" + $("#id").val();
            }
            else {
                formData += "&id=0";
            }
            console.log(formData);
            $.ajax({
                type: "POST",
                url: "/reservations/ReservationOverlaps",
                data: formData,
                dataType: "text",
                success: function (data) {
                    if (data === "False") {
                        allowSubmit = true;
                        $("#frm").submit();
                    }
                    else {
                        $("#overlapExists").show();
                    }
                },
                error: function () {
                    console.log("Error contacting service");
                }
            });
        }
    });
})
