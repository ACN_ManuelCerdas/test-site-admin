﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestSiteAdmin.Models;

namespace TestSiteAdmin.Controllers
{
    public class sitesController : Controller
    {
        private TestSitesManagerEntities db = new TestSitesManagerEntities();

        // GET: sites
        public ActionResult Index()
        {
            var site = db.site.Include(s => s.server);
            return View(site.ToList());
        }
       
        // GET: sites/Create
        public ActionResult Create()
        {
            ViewBag.id_server = new SelectList(db.server, "id", "name");
            return View();
        }

        // POST: sites/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,id_server,login,password")] site site)
        {
            if (ModelState.IsValid)
            {
                db.site.Add(site);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_server = new SelectList(db.server, "id", "name", site.id_server);
            return View(site);
        }

        // GET: sites/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            site site = db.site.Find(id);
            if (site == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_server = new SelectList(db.server, "id", "name", site.id_server);
            return View(site);
        }

        // POST: sites/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,id_server,login,password")] site site)
        {
            if (ModelState.IsValid)
            {
                db.Entry(site).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_server = new SelectList(db.server, "id", "name", site.id_server);
            return View(site);
        }

        // GET: sites/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            site site = db.site.Find(id);
            if (site == null)
            {
                return HttpNotFound();
            }
            return View(site);
        }

        // POST: sites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            site site = db.site.Find(id);
            db.site.Remove(site);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
