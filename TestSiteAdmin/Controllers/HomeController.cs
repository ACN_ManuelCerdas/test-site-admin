﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestSiteAdmin.Models;

namespace TestSiteAdmin.Controllers
{
    public class HomeController : Controller
    {
        private TestSitesManagerEntities db = new TestSitesManagerEntities();

        public ActionResult Index()
        {
            HomeModel homeModel = new HomeModel();

            homeModel.sitios = (
                from a in db.available_sites
                orderby a.name
                select new Sitio { name = a.name, id = a.id }
                ).ToList();


            homeModel.reservaciones = (
                from a in db.active_reservations
                orderby a.site_name,a.start_date,a.end_date
                select new Reservacion
                {
                    name = a.name,
                    description = a.description,
                    start_date = (DateTime)a.start_date,
                    end_date = (DateTime)a.end_date,
                    bo_mail = a.bo_mail,
                    agency_mail = a.agency_mail,
                    user_name = a.user_name,
                    user_email = a.user_email,
                    site = a.site_name,
                    id = a.id
                }
                ).ToList();

            return View(homeModel);
        }

       
    }
}