﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestSiteAdmin.Models;

namespace TestSiteAdmin.Controllers
{
    public class reservationsController : Controller
    {
        private TestSitesManagerEntities db = new TestSitesManagerEntities();

        // GET: reservations
        public ActionResult Index()
        {
            /*
            var reservation = db.reservation.Include(r => r.site).Include(r => r.user);
            return View(reservation.ToList());
            */
            var reservation = (
                from r in db.active_reservations                                
                orderby r.site_name
                select new Reservacion
                {
                    name = r.name,
                    description = r.description,
                    start_date = (DateTime)r.start_date,
                    end_date = (DateTime)r.end_date,
                    bo_mail = r.bo_mail,
                    agency_mail = r.agency_mail,
                    user_name = r.user_name,
                    user_email = r.user_email,
                    site = r.site_name,
                    id = r.id
                }).ToList();
            return View(reservation);
        }
       
        // GET: reservations/Create/5
        public ActionResult Create(int? id)
        {
            if (id == null)
            {
                ViewBag.id_site = new SelectList(db.available_sites.OrderBy(x => x.name), "id", "name");
            }
            else
            {
                ViewBag.id_site = new SelectList(db.available_sites.OrderBy(x => x.name), "id", "name", id);
            }
            ViewBag.id_user = new SelectList(db.user.OrderBy(x => x.name), "id", "name");
            return View();
        }

        // POST: reservations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,description,start_date,end_date,bo_mail,agency_mail,id_user,id_site")] reservation reservation)
        {
            if (ModelState.IsValid)
            {
                db.reservation.Add(reservation);
                db.SaveChanges();
                DateTime start_date = (DateTime)reservation.start_date;
                DateTime end_date = (DateTime)reservation.end_date;

                /* Send an email */
                if (reservation.bo_mail != "")
                {
                    MerckEmailAPI.EmailServiceSoapClient e = new MerckEmailAPI.EmailServiceSoapClient();
                    var mailBody = new System.Text.StringBuilder();
                    mailBody.Append("Dear Sir/Madam");
                    mailBody.Append("A reservation for the site \"" + reservation.name + "\" on the server " + reservation.site + " has been created.");
                    mailBody.Append("The reservation goes from " + start_date.ToString("MM/dd/yyyy") + " to " + end_date.ToString("MM/dd/yyyy") + ".");
                    e.SendPlainEmail("sitemaster@merck.com", reservation.bo_mail, reservation.agency_mail, "", "Test site reservation created", false, mailBody.ToString(), Environment.MachineName, "");
                }

                return RedirectToAction("Index");
            }

            ViewBag.id_site = new SelectList(db.site, "id", "name", reservation.id_site);
            ViewBag.id_user = new SelectList(db.user, "id", "name", reservation.id_user);
            return View(reservation);
        }

        // GET: reservations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            reservation reservation = db.reservation.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_site = new SelectList(db.site.OrderBy(x => x.name), "id", "name", reservation.id_site);
            ViewBag.id_user = new SelectList(db.user.OrderBy(x => x.name), "id", "name", reservation.id_user);
            return View(reservation);
        }

        // POST: reservations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,description,start_date,end_date,bo_mail,agency_mail,id_user,id_site")] reservation reservation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reservation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_site = new SelectList(db.site, "id", "name", reservation.id_site);
            ViewBag.id_user = new SelectList(db.user, "id", "name", reservation.id_user);
            return View(reservation);
        }

        // GET: reservations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            reservation reservation = db.reservation.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            return View(reservation);
        }

        // POST: reservations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            reservation reservation = db.reservation.Find(id);
            db.reservation.Remove(reservation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public bool ReservationOverlaps(int id_site, DateTime start_date, DateTime end_date, int id)
        {
            bool isOverlap = false;
            var reservations = db.ActiveReservation(id_site);

            foreach (var cosa in reservations)
            {
                //A reservation does not overlap itself
                if (id != cosa.id)
                {
                    //Checking for overlaps                
                    isOverlap = (start_date >= cosa.start_date) && (start_date <= cosa.end_date);
                    isOverlap = isOverlap || ((end_date <= cosa.end_date) && (end_date >= cosa.start_date));
                    isOverlap = isOverlap || ((start_date <= cosa.start_date) && (end_date >= cosa.end_date));
                    if (isOverlap)
                    {
                        break;
                    }
                }
            }

            return isOverlap;
        }
    }
}
